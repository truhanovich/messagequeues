﻿using System.Configuration;
using Topshelf;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var messageQueueName = ConfigurationManager.AppSettings["MessageQueueName"].ToString();
            var topicName = ConfigurationManager.AppSettings["TopicName"].ToString();
            var subsName = ConfigurationManager.AppSettings["SubsName"].ToString();
            var messageStatusQueueName = ConfigurationManager.AppSettings["MessageStatusQueueName"].ToString();
            var path = ConfigurationManager.AppSettings["path"].ToString();

            HostFactory.Run(
                hostConf => hostConf.Service<ServiceCentralServer>(
                    s =>
                    {
                        s.ConstructUsing(() => new ServiceCentralServer(messageQueueName, path, topicName, subsName, messageStatusQueueName));
                        s.WhenStarted(serv => serv.Start());
                        s.WhenStopped(serv => serv.Stop());
                    }
                    ));
        }
    }
}
