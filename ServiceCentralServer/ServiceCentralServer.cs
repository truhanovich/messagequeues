﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.IO;
using System.Messaging;
using System.Text;
using System.Threading;

namespace task1
{
    public class ServiceCentralServer
    {
        MessageQueue queue, statusQueue;
        string savePath;
        private readonly ManualResetEvent _stopWorkEvent;
        private StringBuilder loger;

        private readonly Thread _workThread;
        private readonly Thread _acceptStatus;
        private readonly Thread _settings;
        private string topicName, subsName;

        public ServiceCentralServer(string messageQueueName, string path, string topicName, string subsName, string messageStatusQueueName)
        {
            this.topicName = topicName;
            this.subsName = subsName;
            loger = new StringBuilder();
            savePath = path;
            if (MessageQueue.Exists(messageQueueName))
                queue = new MessageQueue(messageQueueName);
            else
                queue = MessageQueue.Create(messageQueueName);

            if (MessageQueue.Exists(messageStatusQueueName))
                statusQueue = new MessageQueue(messageStatusQueueName);
            else
                statusQueue = MessageQueue.Create(messageStatusQueueName);

            _workThread = new Thread(WorkProcedure);
            _acceptStatus = new Thread(AcceptStatusSevice);
            _settings = new Thread(SendSetting);
            _stopWorkEvent = new ManualResetEvent(false);
        }

        private void WorkProcedure(object obj)
        {
            queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(string) });

            do
            {
                using (queue)
                {
                    while (true)
                    {
                        var message = queue.Receive();

                        var bytes = Encoding.ASCII.GetBytes(message.Body.ToString());

                        var path = $@"{savePath}\{message.Label}\";
                        if (Directory.Exists(path) == false)
                        {
                            Directory.CreateDirectory(path);
                        }
                        File.WriteAllBytes($@"{path}\{DateTime.Now.ToString("yyyyMMDD_hhmmss")}.pdf", bytes);
                    }
                }
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent }, 1000) != 0);

        }

        private void AcceptStatusSevice()
        {
            do
            {
                var message = statusQueue.Receive();
                loger.Append($"{DateTime.Now.ToString("yyyy-MMMM-dd:HH-mm-ss")} From: {message.Label}; status: {message.Body}");
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent }, 1000) != 0);
        }

        private void SendSetting()
        {
            var namespaceManager = NamespaceManager.Create();
            var subscription = new SubscriptionDescription(topicName, subsName);
            namespaceManager.CreateSubscription(subscription);
            var client = TopicClient.Create(topicName);

            do
            {
                client.Send(new BrokeredMessage($"length:50;status") { TimeToLive = new TimeSpan(0, 0, 20) });
                Thread.Sleep(1000);
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent }, 1000) != 0);
        }

        public void Start()
        {
            _workThread.Start();
            _acceptStatus.Start();
            _settings.Start();
            
        }

        public void Stop()
        {
            _stopWorkEvent.Set();
            _workThread.Join();
            _acceptStatus.Join();
            _settings.Join();
        }
    }
}
