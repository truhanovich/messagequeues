﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Configuration;
using System.IO;
using System.Threading;
using task1.ConfigManager;

namespace task1
{
    class FileService
    {
        FileSystemWatcher[] watcherList;
        ConfigurationElement[] array;
        Thread workThread;

        ManualResetEvent stopWorkEvent;
        AutoResetEvent newFileEvent;

        public FileService(TargetFoldersConfigSection folders)
        {
            array = new ConfigurationElement[folders.FolderItems.Count];
            watcherList = new FileSystemWatcher[folders.FolderItems.Count];
            folders.FolderItems.CopyTo(array, 0);

            for(var i = 0; i< array.Length; i++)
            {
                var folder = (FolderElement)array[i];
                if (!Directory.Exists(folder.Path))
                    { Directory.CreateDirectory(folder.Path); }
                watcherList[i] = new FileSystemWatcher(folder.Path);
                watcherList[i].Created += Watcher_Created;
            }

            workThread = new Thread(WorkProcedure);
            stopWorkEvent = new ManualResetEvent(false);
            newFileEvent = new AutoResetEvent(false);
        }

        private void WorkProcedure(object obj)
        {
            do
            {
                foreach (FolderElement folder in array)
                {
                    foreach (var file in Directory.EnumerateFiles(folder.Path))
                    {
                        if (stopWorkEvent.WaitOne(TimeSpan.Zero))
                        {
                            return;
                        }

                        //    if (TryOpen(inFile, 3))
                        //        File.Move(inFile, outFile);
                    }
                }

            }
            while (WaitHandle.WaitAny(new WaitHandle[] { stopWorkEvent, newFileEvent }, 1000) != 0);
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            newFileEvent.Set();
        }

        public void Start()
        {
            workThread.Start();
            foreach (var watcher in watcherList)
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        public void Stop()
        {
            foreach (var watcher in watcherList)
            {
                watcher.EnableRaisingEvents = false;
            }
            stopWorkEvent.Set();
            workThread.Join();
        }

        private bool TryOpen(string fileName, int tryCount)
        {
            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    file.Close();

                    return true;
                }
                catch (IOException)
                {
                    Thread.Sleep(5000);
                }
            }

            return false;
        }

        private void CreateDocumetn(string[] pathArray, string pathPdf)
        {
            PdfDocument doc = new PdfDocument();

            foreach (var source in pathArray)
            {
                var page = doc.Pages.Add(new PdfPage());
                XGraphics xgr = XGraphics.FromPdfPage(page);
                XImage img = XImage.FromFile(source);
                xgr.DrawImage(img, 0, 0, img.PixelWidth, img.PixelHeight);
            }

            doc.Save(pathPdf);
            doc.Close();
        }

    }
}
