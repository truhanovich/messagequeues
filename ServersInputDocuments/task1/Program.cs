﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using task1.ConfigManager;
using Topshelf;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var inDir = Path.Combine(currentDir, "in");
            var outDir = Path.Combine(currentDir, "out");

            TargetFoldersConfigSection folders = (TargetFoldersConfigSection)ConfigurationManager.GetSection("TargetFolders");

            CreateDocumetn( new[]{ @"d:\Temp\image_0.jpg ", @"d:\Temp\image_1.jpg " }, @"d:\Temp\1.pdf");

            //HostFactory.Run(
            //    hostConf => hostConf.Service<FileService>(
            //        s =>
            //        {
            //            s.ConstructUsing(() => new FileService(folders));
            //            s.WhenStarted(serv => serv.Start());
            //            s.WhenStopped(serv => serv.Stop());
            //        }
            //        ));
        }

        private static void CreateDocumetn(string[] pathArray, string pathPdf)
        {
            PdfDocument doc = new PdfDocument();

            foreach (var source in pathArray)
            {
                var page = doc.Pages.Add(new PdfPage());
                XGraphics xgr = XGraphics.FromPdfPage(page);
                XImage img = XImage.FromFile(source);
                xgr.DrawImage(img, 0, 0,img.Width,img.Height);
            }


            doc.Save(pathPdf);
            doc.Close();

        }
    }
}
