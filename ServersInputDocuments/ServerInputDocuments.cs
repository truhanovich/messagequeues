﻿using Microsoft.ServiceBus.Messaging;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using task1.ConfigManager;

namespace task1
{
    class ServerInputDocuments
    {
        private readonly FileSystemWatcher[] _watcherList;
        private readonly ConfigurationElement[] _array;
        private readonly Thread _workThread;
        private readonly Thread _settings;
        private readonly Thread _sendStatus;

        private readonly ManualResetEvent _stopWorkEvent;
        private readonly AutoResetEvent _newFileEvent;

        private readonly string outFile = "d:\\temp";

        private int length;

        private string status;

        private string topicName;
        private string subsName;

        MessageQueue queue, statusQueue;

        public ServerInputDocuments(
            TargetFoldersConfigSection folders, 
            string messageQueueName, 
            string topicName, 
            string subsName, 
            string messageStatusQueueName)
        {
            this.topicName = topicName;
            this.subsName = subsName;
            status = "Starting";
            length = 30;
            if (MessageQueue.Exists(messageQueueName))
                queue = new MessageQueue(messageQueueName);
            else
                queue = MessageQueue.Create(messageQueueName);

            if (MessageQueue.Exists(messageStatusQueueName))
                statusQueue = new MessageQueue(messageStatusQueueName);
            else
                statusQueue = MessageQueue.Create(messageStatusQueueName);

            _array = new ConfigurationElement[folders.FolderItems.Count];
            _watcherList = new FileSystemWatcher[folders.FolderItems.Count];
            folders.FolderItems.CopyTo(_array, 0);

            for (var i = 0; i < _array.Length; i++)
            {
                var folder = (FolderElement)_array[i];
                if (!Directory.Exists(folder.Path))
                {
                    Directory.CreateDirectory(folder.Path);
                }
                _watcherList[i] = new FileSystemWatcher(folder.Path);
                _watcherList[i].Created += Watcher_Created;
            }

            _workThread = new Thread(WorkProcedure);
            _settings = new Thread(ReadSettings);
            _sendStatus = new Thread(SendStatus);
            _stopWorkEvent = new ManualResetEvent(false);
            _newFileEvent = new AutoResetEvent(false);
        }

        private bool TryOpen(string fileName, int tryCount)
        {
            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    file.Close();

                    return true;
                }
                catch (IOException)
                {
                    Thread.Sleep(5000);
                }
            }

            return false;
        }

        private void CreateDocument(List<string> pathArray, string pathPdf)
        {
            status = "Create document";
            var doc = new PdfDocument();

            foreach (var source in pathArray)
            {
                var page = doc.Pages.Add(new PdfPage());
                var xgr = XGraphics.FromPdfPage(page);
                var img = XImage.FromFile(source);
                xgr.DrawImage(img, 0, 0, img.PixelWidth, img.PixelHeight);
            }

            doc.Save(pathPdf);
            doc.Close();

            var objSR = File.ReadAllBytes(pathPdf);
            var str = Encoding.ASCII.GetString(objSR);
            queue.Send(str, $"{Environment.MachineName}");
            File.Delete(pathPdf);
        }

        private List<string> GetListPaths(List<string> start)
        {
            var resul = new List<string>();
            if (start.Count < 1)
            {
                return resul;
            }

            var previousNum = int.Parse(start[0].Split('_')[1].Split('.')[0]);
            for (var index = 1; index < start.Count; index++)
            {
                var curentNym = int.Parse(start[index].Split('_')[1].Split('.')[0]);
                if (curentNym == previousNum + 1)
                {
                    resul.Add(start[index]);
                    previousNum = curentNym;
                }
                else
                {
                    return resul;
                }
            }

            return resul;
        }

        private bool IsRefresh(IEnumerable<string> listFile, TimeSpan wait)
        {
            var msSecondForWait = new TimeSpan(0);
            foreach (var path in listFile)
            {
                var createTime = new FileInfo(path).CreationTime;
                if (createTime + wait > DateTime.Now && DateTime.Now - createTime > msSecondForWait)
                {
                    msSecondForWait = DateTime.Now - createTime;
                }
            }

            if (msSecondForWait == TimeSpan.Zero)
            {
                return false;
            }

            Thread.Sleep(msSecondForWait);
            return true;
        }

        private void CreatePdfOrBackUp(List<string> result, string path, string name)
        {
            if (result.Any(p => p.Split('.')[1] != "jpg"))
            {
                foreach (string itme in result)
                {
                    if (TryOpen(itme, 3))
                    {
                        File.Copy(itme, outFile);
                    }
                }
            }
            else
            {
                CreateDocument(result, path + name + ".pdf");
            }

        }

        private void WorkProcedure(object obj)
        {
            do
            {
                foreach (var configurationElement in _array)
                {
                    var folder = (FolderElement)configurationElement;
                    var fileList = Directory.EnumerateFiles(folder.Path).ToList();

                    while (fileList.Any())
                    {
                        string first = fileList[0];
                        var patternOfName = first.Split('_');
                        var imageListForPdf = fileList.Where(p => p.Contains(patternOfName[0])).ToList();
                        imageListForPdf.Sort();

                        var result = GetListPaths(imageListForPdf);

                        if (IsRefresh(result, new TimeSpan(0, 0, 0, length)))
                        {
                            fileList = Directory.EnumerateFiles(folder.Path).ToList();
                            continue;
                        }

                        CreatePdfOrBackUp(result, folder.Path, patternOfName[0]);

                        foreach (string itme in result)
                        {
                            if (TryOpen(itme, 3))
                            {
                                File.Delete(itme);
                            }
                        }

                        fileList = Directory.EnumerateFiles(folder.Path).ToList();
                    }
                }

                status = "Wait";
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _newFileEvent }, 1000) != 0);
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            _newFileEvent.Set();
        }

        private void ReadSettings(object obj)
        {
            var client = SubscriptionClient.Create(topicName, subsName, ReceiveMode.ReceiveAndDelete);
            do
            {
                var message = client.Receive();

                var settings = message.GetBody<string>().Split(';');
                var setting = settings.FirstOrDefault(t => t.Contains("length"));

                if (string.IsNullOrWhiteSpace(setting) == false)
                {
                    length = int.Parse(setting.Split(':')[1]);
                }

                setting = settings.FirstOrDefault(t => t.Contains("status"));
                if (string.IsNullOrWhiteSpace(setting) == false)
                {
                    SendStatusSevice();
                }
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _newFileEvent }, 1000) != 0);
        }

        private void SendStatus(object obj)
        {
            do
            {
                SendStatusSevice();
                Thread.Sleep(10000);
            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _newFileEvent }, 1000) != 0);
        }

        private void SendStatusSevice()
        {
            statusQueue.Send(status, $"{Environment.MachineName}");
        }

        public void Start()
        {
            _workThread.Start();
            _settings.Start();
            _sendStatus.Start();
            foreach (var watcher in _watcherList)
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        public void Stop()
        {
            foreach (var watcher in _watcherList)
            {
                watcher.EnableRaisingEvents = false;
            }
            _stopWorkEvent.Set();
            _workThread.Join();
            _settings.Join();
            _sendStatus.Join();
        }
    }
}
