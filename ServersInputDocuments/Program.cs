﻿using System.Configuration;
using task1.ConfigManager;
using Topshelf;

namespace task1
{
    class Program
    {
        static void Main()
        {
            var folders = (TargetFoldersConfigSection)ConfigurationManager.GetSection("TargetFolders");
            var messageQueueName = ConfigurationManager.AppSettings["MessageQueueName"].ToString();
            var topicName = ConfigurationManager.AppSettings["TopicName"].ToString();
            var subsName = ConfigurationManager.AppSettings["SubsName"].ToString();
            var messageStatusQueueName = ConfigurationManager.AppSettings["MessageStatusQueueName"].ToString();

            HostFactory.Run(
                hostConf => hostConf.Service<ServerInputDocuments>(
                    s =>
                    {
                        s.ConstructUsing(() => new ServerInputDocuments(folders, messageQueueName, topicName, subsName, messageStatusQueueName));
                        s.WhenStarted(serv => serv.Start());
                        s.WhenStopped(serv => serv.Stop());
                    }
                    ));
        }
    }
}
